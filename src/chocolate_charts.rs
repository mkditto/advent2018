use std::fmt;


type Recipe = u16;

pub struct Elf {
    recipe : Recipe,
}

pub struct Scoreboard {
    elves : [Elf ; 2],
    scoreboard : Vec<Recipe>,
}


impl Elf {
    fn new(recipe : Recipe) -> Elf {
        Elf {
            recipe : recipe,
        }
    }
}


impl Scoreboard {
    fn new(elf_one_r : Recipe, elf_two_r : Recipe) -> Scoreboard {
        Scoreboard {
            elves : [Elf::new(elf_one_r), Elf::new(elf_two_r)],
            scoreboard : vec![elf_one_r, elf_two_r],
        }
    }

    fn advance_elf(&mut self, elf_num : usize) {
        self.elves[elf_num].recipe = 
            (self.elves[elf_num].recipe + 1) % (self.scoreboard.len() as Recipe);
    }
}


impl fmt::Display for Scoreboard {
    fn fmt(&self, f : &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.scoreboard.iter().map(|score| {
                format!("{}", score)
            }).collect::<String>())
    }
}


fn main() {
    let _coco = Scoreboard::new(3, 7);
}
